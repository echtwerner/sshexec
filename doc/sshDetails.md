# SSH Details

This Document describes the current understanding I gained with SSH so far. Is mend to be a mind dump for me. So not hassle with grammar etc. My brain is the parser right now :-)


## Entry Point

Summary of all RFC and additional information can be found under  https://www.openssh.com/specs.html and https://godoc.org/golang.org/x/crypto/ssh for golang


## SSH Overview

In RFC https://tools.ietf.org/html/rfc4251 Section 9.3 - 9.5 the main Building Blocks of an ssh connection is described.

### 1. Transport Protocol 
* https://tools.ietf.org/html/rfc4251#section-9.3
* https://tools.ietf.org/html/rfc4253



* The transport protocol provides a confidential channel over an insecure network.
* It performs server host authentication, key exchange, encryption, and integrity protection. 
* It also derives a unique session id that may be used by higher-level protocols.

I don't dive to deep into this Topic


### 2. Authentication Protocol
* https://tools.ietf.org/html/rfc4251#section-9.4
* https://tools.ietf.org/html/rfc4251#section-9.4


* The purpose of this protocol is to perform client user authentication.  
* It Assumes it runs over Secured Transport Protocol

I don't dive to depp into this Topic

### 3. Connection Protocol
* https://tools.ietf.org/html/rfc4251#section-9.5
* https://tools.ietf.org/html/rfc4254


The Connection Protocol is the interessting Area for this Project. And is described in more detail in rfc4254.

#### Golang

ssh.Dial() https://godoc.org/golang.org/x/crypto/ssh#Client.Dial is the Entry Point to create a Connection of Conn type https://godoc.org/golang.org/x/crypto/ssh#Conn defined in connection.go. 
The connection will be the basis to create a session.

* connection.SendRequest() is relevant to send "Global Request" (rfc4254 Section 4). 
* It seems that connection.go is also responsible for creating the diffent channels (channel.go and Section 5). The channel are multiplex by mux.go.
* channel.SendRequest() send the chanreq (SSH_MSG_CHANNEL_REQUEST Section 6.2 + 6.5) for session.go.
* session.RequestPty() sends chanreq "pty-req" (Sec 6.2)
* session.Run()->Start() sends chanreq "exec" (Sec 6.5)
* session.Shell sends chanreq "shell"


 
## Input Readings

### Starting Point

* https://kukuruku.co/post/ssh-commands-execution-on-hundreds-of-servers-via-go/

### Basics

* https://godoc.org/golang.org/x/crypto/ssh
* https://godoc.org/net#Conn
* https://github.com/golang/crypto/blob/master/ssh/



### Examples
* https://github.com/gl-prototypes/ssh-client
* https://github.com/YuriyNasretdinov/GoSSHa
* https://gist.github.com/artyom/1a7d46c9511dd93b0714baf7b9745605




## References 


* https://web.archive.org/web/20170215184048/https://blogs.oracle.com/janp/entry/how_the_scp_protocol_works
