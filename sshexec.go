package sshexec

//This File will be the Library. Currently I stay with package main.

import (
	"fmt"
	"io/ioutil"
	"log"
	"time"

	"golang.org/x/crypto/ssh"
)

//helper
func check(e error) {
	if e != nil {
		log.Fatal(e)
	}
}

// sshExecute connects to remote ssh server, prepares a session,
// calls the executeHandler and returns the collected stdout.
// The executeHandler gets the session object and can be used
// to call Run() or Shell() within the session.
func sshExecute(hostname string, config *ssh.ClientConfig, executeHandler func(*ssh.Session)) string {
	// Connect to ssh server
	conn, err := ssh.Dial("tcp", hostname+":22", config) //conn fullfills type ssh/connection/Conn interafce and is also a type ssh/client/Client struct
	check(err)
	defer conn.Close()

	// Create a session
	session, err := conn.NewSession()
	check(err)
	defer session.Close()

	// Get Stdout from remotesite
	outpipe, err := session.StdoutPipe()
	check(err)
	// ssh Run() or ssh Shell() is currently executed and outpipe will get response
	executeHandler(session)

	// reading Outpipe into string
	allout, err := ioutil.ReadAll(outpipe)
	check(err)
	return hostname + ": " + string(allout)
}

func executeCmd(cmd, hostname string, config *ssh.ClientConfig) string {
	return sshExecute(hostname, config, func(session *ssh.Session) {
		session.Run(cmd)
	})
}

func executeShell(cmd, hostname string, config *ssh.ClientConfig) string {
	return sshExecute(hostname, config, func(session *ssh.Session) {
		// Prepare pipe for command input.
		w, err := session.StdinPipe()
		check(err)
		// Prepare stderr pipe to print error if command generates an error
		serr, err := session.StderrPipe()
		check(err)

		// Start remote shell
		if err := session.Shell(); err != nil {
			log.Fatal("failed to start shell: ", err)
		}

		//Feed in the command(s).
		w.Write([]byte("\n"))
		w.Write([]byte(cmd + "\n"))
		w.Close()

		//Check for errors and print them out
		errout, err := ioutil.ReadAll(serr)
		check(err)
		fmt.Print(string(errout))

		session.Wait()
	})
}

func executeShellPty(cmd, hostname string, config *ssh.ClientConfig) string {
	return sshExecute(hostname, config, func(session *ssh.Session) {
		// Prepare pipe for command input.
		w, err := session.StdinPipe()
		check(err)
		// Prepare stderr pipe to print error if command generates an error
		serr, err := session.StderrPipe()
		check(err)
		// Set up terminal modes
		modes := ssh.TerminalModes{
			ssh.ECHO:          0,     // disable echoing
			ssh.TTY_OP_ISPEED: 14400, // input speed = 14.4kbaud
			ssh.TTY_OP_OSPEED: 14400, // output speed = 14.4kbaud
		}
		// Request pseudo terminal
		if err := session.RequestPty("xterm", 100, 500, modes); err != nil {
			log.Fatal("request for pseudo terminal failed: ", err)
		}
		// Start remote shell
		if err := session.Shell(); err != nil {
			log.Fatal("failed to start shell: ", err)

		}
		//Feed in Newline / Return and wait long enough to give NetIron OS some time.
		//with 100ms the behavoir of an MLX wasn't consistent.
		w.Write([]byte("\n"))
		time.Sleep(200 * time.Millisecond)
		//Feed in the command(s).
		w.Write([]byte(cmd + "\n"))
		w.Write([]byte("exit\n"))
		w.Close()
		//Check for errors and print them out
		errout, err := ioutil.ReadAll(serr)
		check(err)
		fmt.Print(string(errout))

		session.Wait()
	})
}

// Entrypoint for cmdline Tool
func Run(user, passwd, cmd, host string, useShell, usePty bool) error {
	hosts := []string{host}                // later a list of hosts should be implemented, so we take host to process things
	results := make(chan string, 10)       // we’ll write results into the buffered channel of strings
	timeout := time.After(5 * time.Second) // in 5 seconds the message will come to timeout channel

	// initialize the structure with the configuration for ssh package.
	config := &ssh.ClientConfig{
		User: user,
		Auth: []ssh.AuthMethod{
			ssh.Password(passwd),
		},
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
	}

	// running one goroutine (light-weight alternative of OS thread) per server,
	for _, hostname := range hosts {
		go func(hostname string) {
			if useShell {
				results <- executeShell(cmd, hostname, config)

			} else if usePty {
				results <- executeShellPty(cmd, hostname, config)
			} else {
				results <- executeCmd(cmd, hostname, config)
			}
		}(hostname)
	}

	// collect results from all the servers or print "Timed out",
	// if the total execution time has expired
	for i := 0; i < len(hosts); i++ {
		select {
		case res := <-results:
			fmt.Print(res)
		case <-timeout:
			return fmt.Errorf("Time out!")
		}
	}
	return nil
}
