//Test command line tool for development

package main

//This is only for cmdline parts. After PoC is converted to library this will be just a usage exampel

import (
	"flag"
	"./src/sshexec"
	"github.com/artyom/autoflags"

)

//helper


import (
	"log"
)

func check(e error) {
	if e != nil {
		log.Fatal(e)
	}
}



func main() {
	//Setting up command line args
	args := struct {
		User     string `flag:"l, connect with specified username"`
		Password string `flag:"pw, login with specified password"`
		Command  string `flag:"c, command to execute"`
		Server   string `flag:"s, server to connect to"`
		UseShell bool   `flag:"S, use shell to execute"`
		UsePty   bool   `flag:"P, use shell to execute"`
	}{
		User:     "",
		Password: "",
		Command:  "",
		Server:   "",
		UseShell: false,
		UsePty:   false,
	}
	autoflags.Define(&args)
	flag.Parse()
	//run programm
	err := sshexec.Run(args.User, args.Password, args.Command, args.Server, args.UseShell, args.UsePty)
	check(err)
}
